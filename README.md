# TEST NODEJS
# INTRODUCTION
-   Phân tích thiết kế một hệ thống có các chức năng CURD user. Hệ thống cần có chức năng login, phân cấp quyền cho user ứng với từng chức năng. Sử dụng grapql
-	Sử dụng thông tin login để thống kê số lượng user mới và số lượng user hoạt động theo ngày.
# INSTALL 
-   npm install 
# USE
-   Tạo database mongoDB thêm URL vào file .env
-   Import users.json trong folder data 
-       admin: email(admin@gmail.com)
             : password(123456)
-       user: email(user@gmail.com)
             : password(123456)
-   npm run server
-   Để xác thực quyền cần gán token vào header 'Bearer token'
# Feature
-   Login
-   Register   
-   logout 
-   CRUD user
-   Đếm số lượng user mới theo ngày
-   Đếm số lượng user online
