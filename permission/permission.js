const {rule, shield, or, and } = require("graphql-shield");
const jwt = require('jsonwebtoken');

const isAuth = rule()((parent, args, context, info) => {
  const token = context.req.headers.authorization;
  const user = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  let check = false
  if (user.id === args.id || user.email === args.email) {
    check = true
  }
  return !!check
})
const isAdmin = rule()((parent, args, context, info) => {
  const token = context.req.headers.authorization;
  const user = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  return !!user && user.isAdmin === true
})

const permissions = shield(
  {
    Query: {
      logout: isAuth,
    },
    Mutation: {
      resetPassword: isAuth,
    }
  },
  {
    allowExternalErrors: true,
  }
);

module.exports = { permissions };
