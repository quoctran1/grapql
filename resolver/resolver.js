const User = require("../models/User");
const jwt = require("jsonwebtoken");
const { UserInputError } = require("apollo-server-express");
const { validateInput } = require("../utils/validator");
const bcrypt = require("bcryptjs");

function generateToken(user) {
  return jwt.sign(
    {
      id: user._id,
      email: user.email,
      isAdmin: user.isAdmin,
    },
    process.env.ACCESS_TOKEN_SECRET,
    { expiresIn: "20m" }
  );
}

const resolvers = {
  // QUERY
  Query: {
    logout: async (_, args, context) => {
      context.req.headers.authorization = null;
      return "Logout";
    },
  },

  // MUTATION
  Mutation: {
    login: async (_, { email, password }) => {
      const { errors, valid } = validateInput(email, password);

      if (!valid) {
        throw new UserInputError("Errors", { errors });
      }

      const user = await User.findOne({ email });

      if (!user) {
        errors.general = "User not found";
        throw new UserInputError("User not found", { errors });
      }

      const match = await bcrypt.compare(password, user.password);
      if (!match) {
        errors.general = "Wrong password";
        throw new UserInputError("Wrong password", { errors });
      }
      const token = generateToken(user);
      return token;
    },

    // Create user
    register: async (parent, { email, password, name, age, address }) => {
      const { valid, errors } = validateInput(email, password);
      console.log("OK");
      if (!valid) {
        throw new UserInputError("Errors", { errors });
      }
      // TODO: Make sure user doesnt already exist
      const user = await User.findOne({ email });
      if (user) {
        throw new UserInputError("email is taken", {
          errors: {
            email: "This email is taken",
          },
        });
      }
      const newUser = await User.create({
        email: email,
        password: await bcrypt.hash(password, 12),
        name: name,
        age: age,
        address: address,
      });
      // const res = await newUser.save();
      return newUser;
    },

    // Reset Password
    resetPassword: async(_, { email, password}) => {
      const { errors, valid } = validateInput(email, password);
      if (!valid) {
        throw new UserInputError("Errors", { errors });
      }
      const user = await User.findOne({ email });
      if (!user) {
        throw new UserInputError("User not found", { errors });
      }
      user.password = await bcrypt.hash(password, 12)
      await user.save()
    }
  },
};

module.exports = resolvers;
