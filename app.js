require("dotenv").config();
const { ApolloServer, gql } = require("apollo-server");
const fs = require("fs");
const { buildFederatedSchema } = require("@apollo/federation");
const { applyMiddleware } = require("graphql-middleware");
const { permissions } = require("./permission/permission");
const mongoose = require("mongoose");

// Load schema & resolvers
const typeDefs = gql`
  ${fs.readFileSync(__dirname.concat("/schema.graphql")).toString()}
`;
const resolvers = require("./resolver/resolver");

// Connect to MongoDB
const connectDB = async () => {
  try {
    await mongoose.connect(process.env.MONGODB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Connect Successfully");
  } catch (error) {
    console.log("Connect Failure");
  }
};

connectDB();

const server = new ApolloServer({
  schema: applyMiddleware(
    buildFederatedSchema([{ typeDefs, resolvers }]),
    permissions
  ),
  context: ({ req }) => {
    return {
      req,
    };
  },
});
server.listen(process.env.PORT).then(({ url }) => {
  console.log(`server ready at ${url}`);
});
